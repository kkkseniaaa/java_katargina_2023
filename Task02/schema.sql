drop table if exists client,"order";
drop table if exists driver,car;

create table client (
                        idOfClient bigserial primary key,
                        firstName varchar (20) not null ,
                        lastName varchar (20),
                        email varchar(30) not null ,
                        password char (10) not null
);

create table driver (
                        idOfDriver bigserial primary key,
                        firstName varchar (20) not null ,
                        lastName varchar (20),
                        numberOfDriverLicense bigint not null
);


create table car (
                    idOfCar bigserial primary key,
                    color varchar (20) not null,
                    model varchar (30) not null,
                    number char(6) not null
);

create table "order" (
                  tripStartTime time not null,
                  tripEndTime time not null,
                  tripPrice int

);
alter table driver add rating smallint not null default 1;

alter table car add driverId bigserial;
alter table car add foreign key (driverId) references driver(idOfDriver);

alter table "order" add carId bigserial;
alter table "order" add clientId bigserial;
alter table "order" add driverId bigserial;
alter table "order" add foreign key(clientId) references client(idOfClient);
alter table "order" add foreign key(driverId) references driver(idOfDriver);
alter table "order" add foreign key(carId) references car(idOfCar);




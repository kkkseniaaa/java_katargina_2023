insert into client(firstName, lastName, email, password) values ('Ксения', 'Катаргина','ksekat@mail.ru' , 'qwerty0007');
insert into client(firstName, lastName, email, password) values ('Айри','Латыпкова', 'airimairi@gmail.com','airina2003');
insert into client(firstName, lastName, email, password) values ('Лена','Гаврилкова', 'lgvrvl@gmail.com','leno4ka543');

update client set email = 'neairi2005@mail.ru' where idofclient = 2;

insert into driver(firstName, lastName, numberOfDriverLicense) values ('Денис', 'Пырытков',7414292010);
insert into driver(firstName, lastName, numberOfDriverLicense) values ('Лиза','Голодова', 7414893456);
insert into driver(firstName, lastName, numberOfDriverLicense,rating) values ('Татьяна','Кырылкова',7414895432, 4);

update driver set rating = 4 where firstname = 'Денис';
update driver set numberofdriverlicense = 89936245211 where idofdriver = 1;

insert into car(color, model, number) values ('black', 'mazda', 'А777АА');
insert into car(color, model, number) values ('white', 'lada2114', 'А123УФ');
insert into car(color, model, number) values ('grey', 'lada2110', 'К789ТО');

update car set color  = 'blue' where number = 'A777AA';

insert into "order" (tripStartTime, tripEndTime, tripPrice) values ('12:26:00', '12:50:03', 156);
insert into "order" (tripStartTime, tripEndTime, tripPrice) values ('11:15:00', '12:00:15', 734);
insert into "order" (tripStartTime, tripEndTime, tripPrice) values ('15:00:00', '15:27:09', 520);


select model,speed,hd
from PC
where price < 500

select distinct s.maker as maker
from Product s
where s.type = 'Printer'

select model, ram, screen
from Laptop
where price > 1000

select *
from Printer
where color = 'y'

select s.model as model , s.speed as speed , s.hd as hd
from PC s
where (s.cd = '12x' or s.cd = '24x') and s.price < 600

select distinct p.maker as maker , l.speed as laptop
from Product p, Laptop l
where l.hd >= 10 and l.model = p.model

select s.model, s.price
from (
    select model, price
    from PC
    union select model, price
    from Laptop
    union select model, price
    from Printer) as s
    join Product p on s.model = p.model
where p.maker = 'B'

select distinct p.maker as maker
from Product p
    inner join PC s on p.model = s.model
where s.speed >= 450

select model, price
from Printer
where price = (
    select max(price)
    from Printer)

select avg(speed)
from PC

select avg(speed)
from Laptop
where price > 1000

select avg(s.speed)
from PC s, Product p
where s.model = p.model and p.maker = 'A'

select s.class, s.name, c.country
from ships s
    left join classes c on s.class = c.class
where c.numGuns >= 10

select hd
from PC
group by(hd) having count (model) >= 2

select distinct p.type,p.model,l.speed
from Laptop l
    join Product p on l.model = p.model
where l.speed<(
    select min(speed)
    from PC)

select distinct p.maker, s.price
from Product p, Printer s
where p.model = s.model and s.color = 'y' and s.price = (
    select min(s.price)
    from Printer s
    where s.color = 'y')

select p.maker, avg(s.screen)
from Product p
    join Laptop s on p.model = s.model
group  by maker

select maker, count(model)
from Product
where type='PC'
group by maker having count(model)>=3

select p.maker, max(pc.price)
from Product p
    join PC pc on p.model = pc.model
group by maker

select speed, avg(price)
from PC
where speed > '600'
group by speed

select p.maker
from Product p
    join PC pc on p.model = pc.model
where pc.speed >= '750'
intersect select p.maker
from Product p
    join Laptop l on p.model = l.model
where l.speed >= '750'

select distinct p.maker
from Product p
    join PC pc on p.model = pc.model
where pc.ram = (
    select min(ram)
    from PC)
  and pc.speed =(
      select max(speed)
      from PC
      where ram = (
          select min(ram)
          from PC))
  and p.maker in (
      select maker
      from Product
      where type = 'Printer')

select maker, avg(hd)
from Product p
    join PC s on p.model=s.model
where maker in(
    select maker
    from Product
    where type ='Printer')
group by maker

select count(maker)
from Product
where maker in (
    select maker
    from Product
    group by maker having count(model) = 1)

select distinct class, country
from Classes
where bore >= 16

select o.ship
from Battles b
    left join Outcomes o on o.battle = b.name
where b.name = 'North Atlantic' and o.result = 'sunk'

select s.name
from Classes c,Ships s
where s.launched >=1922 and c.displacement > 35000 and c.type='bb' and s.class = c.class

select p.model, p.type
from Product p
where p.model not like '%[^0-9]%' or p.model not like '%[^A-Z]%'

select o.ship
from Outcomes o
    join Battles b on o.battle=b.name
where result not in ('damaged') and exists(
    select ship
    from Outcomes
        join Battles on battle = name
    where ship = o.ship and b.date > date and result in ('damaged'))

select maker, max(type)
from Product
group by maker having count (distinct type) = 1 and count(model) > 1

select ship, battle
from Outcomes
where result = 'sunk'

select name
from battles
where year(date) not in (
select launched
from ships
where launched is not null)

select name
from Ships
where name like 'R%'
union select Ship
from Outcomes
where Ship like 'R%'

select name
from Ships
where name like '% % %'
union select Ship
from Outcomes
where Ship like '% % %'

select cl.class
from Classes cl
    left join Ships s on s.class = cl.class
where cl.class in (
    select ship
    from Outcomes
    where result = 'sunk')
   or s.name in (
       select ship
       from Outcomes
       where result = 'sunk')
group by cl.class

select s.name
from Classes c
    join Ships s on c.class = s.class
where bore = 16
union select o.ship
from Outcomes o
    join Classes c on c.class = o.ship
where bore = 16

select distinct battle
from Outcomes
where ship in (
    select name
    from Ships
    where class = 'kongo')

select s.name
from Ships s
    join Classes c on c.class=s.class
where country = 'Japan' and type = 'bb' and (numguns >= 9 or numguns is null) and (bore < 19 or bore is null) and (displacement <= 65000 or displacement is null)

select c.class, count(s.ship)
from Classes c
    left join(
        select o.ship, sh.class
        from Outcomes o
            left join Ships sh on sh.name = o.ship
        where o.result = 'sunk') as s on s.class = c.class or s.ship = c.class
group by c.class

select name
from Passenger
where ID_psg in (
    select ID_psg
    from Pass_in_trip
    group by place, ID_psg having count(*)>1)

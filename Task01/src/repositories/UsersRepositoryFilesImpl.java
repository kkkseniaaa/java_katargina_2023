package repositories;

import models.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

public class UsersRepositoryFilesImpl implements UsersRepository {
    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        Scanner sc1 = null;
        try {
            sc1 = new Scanner(Paths.get(fileName));
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<User> users = new ArrayList<>();
        while (true) {
            assert sc1 != null;
            if (!sc1.hasNextLine()) break;
            String s = sc1.nextLine();
            String[] date = s.split("\\|");
            users.add(new User(date[0], date[1], date[2], date[3], date[4]));
        }

        return users;
    }

    @Override
    public void save(User entity) {
        entity.setId(UUID.randomUUID().toString());
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        deleteById(entity.getId());
        save(entity);
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(String aLong) {
        try {
            Path input = Paths.get(fileName);
            Path temp = Files.createTempFile("temp", ".txt");
            Stream<String> lines = Files.lines(input);
            try (BufferedWriter writer = Files.newBufferedWriter(temp)) {
                lines
                        .filter(line -> !line.startsWith(aLong))
                        .forEach(line -> {
                            try {
                                writer.write(line);
                                writer.newLine();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        });
            }

            Files.move(temp, input, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(String aLong) {
        User user = null;
        for (User u : findAll()) {
            if (u.getId().equals(aLong)) {
                user = u;
            }
        }
        return user;
    }
}

package app;

import dto.SignUpForm;
import mappers.Mappers;
import models.User;
import repositories.UsersRepository;
import repositories.UsersRepositoryFilesImpl;
import services.UsersService;
import services.UsersServiceImpl;


public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));
        usersService.signUp(new SignUpForm("Илья", "Ивлев",
                "ivlev2005@gmail.com", "новыйпароль"));
        usersService.signUp(new SignUpForm("Таня", "Крылкова",
                "bestgirlintheworld@gmail.com", "1746"));
        usersService.signUp(new SignUpForm("Далия", "Сафина",
                "lubluhorses@gmail.com", "конина123"));
        usersService.signUp(new SignUpForm("Лена", "Гаврилова",
                "chocolate@gmail.com", "lgvrvl"));
        usersService.signUp(new SignUpForm("Айрина", "Латыпова",
                "pythonistka@gmail.com", "hochuspat"));
        System.out.println(usersRepository.findAll());
        usersRepository.deleteById(usersRepository.findAll().get(0).getId());
        usersRepository.delete(usersRepository.findById(usersRepository.findAll().get(1).getId()));
        System.out.println(usersRepository.findById(usersRepository.findAll().get(3).getId()));
        User user = new User(usersRepository.findAll().get(2).getId(),"Ксения","Катаргина", "ksenia@gmail.com","vsemprivet" );
        usersRepository.update(user);
    }
}

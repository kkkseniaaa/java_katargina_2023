package mappers;

import dto.SignUpForm;
import models.User;

public class Mappers {
    public static User fromSignUpForm(SignUpForm form) {
        return new User(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
    }
}

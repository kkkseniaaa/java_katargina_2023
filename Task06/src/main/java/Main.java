

import jdbc.SimpleDataSource;
import models.Student;
import repositories.StudentsRepository;
import repositories.StudentsRepositoryJdbcTemplateImpl;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);

        Student student = Student.builder()
                .firstName("Hi!")
                .lastName("Bye")
                .email("email")
                .password("qwerty0")
                .build();

        System.out.println(student);
        studentsRepository.save(student);
        System.out.println(student);

        studentsRepository.delete(1L);
        System.out.println(studentsRepository.findAllByAgeGreaterThanOrderByIdDesc(21));
        Student newStudent = Student.builder()
                .id(2L)
                .firstName("Lena")
                .lastName("Gavrilova")
                .age(22)
                .email("lgvrl2003@mail.ru")
                .password("bubblegum2002")
                .build();
        studentsRepository.update(newStudent);
    }

}
